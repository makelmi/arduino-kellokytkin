/*
 * Projektityö
 *
 * ARDUINO-KELLOKYTKIN
 *
 * Mäkelä, Palmunen, Rajanen, Soini
 *
 */



// VAKIOT
const int ANODES[4] = {6,5,4,3};   // Näyttöjen anodit
const int CLOCK = 7;        // 74HC595 Clock
const int LATCH = 8;        // 74HC595 Latch
const int DATA = 9;         // 74HC595 Data

const int PLUSBTN = 12;     // Plusnapin dPin
const int MINUSBTN = 1;     // Miinusnapin dPin
const int ENTERBTN = 2;     // Enternapin dPin
const int LED1 =  10;       // Kelloledi
const int RELAY =  11;      // Rele + aktiiviledi
const int TMPSENSOR = 0;    // Lämpötila-anturi TMP36

const long DEBOUNCEDELAY = 50;    // Kytkimien debounce-viive
const long MENUDELAY = 5000;      // Valikkoviive (5000 = 5s)

const byte DIGITS[13] = {         // Numerot & merkit tavuina
  B00111111,  // 0 & O
  B00000110,  // 1
  B01011011,  // 2
  B01001111,  // 3
  B01100110,  // 4
  B01101101,  // 5
  B01111101,  // 6
  B00000111,  // 7
  B01111111,  // 8
  B01101111,  // 9
  B01110111,  // A
  B00111110,  // U
  B01111000,  // T
};

const int LENGTHS[9][4] = {     // Lämmitysajat (viimeinen = auto)
  {0, 0, 0, 0},
  {0, 1, 0, 0},
  {0, 1, 3, 0},
  {0, 2, 0, 0},
  {0, 2, 3, 0},
  {0, 3, 0, 0},
  {0, 3, 3, 0},
  {0, 4, 0, 0},
  {10, 11, 12, 0}
};

typedef enum {
  PRGM_STATE_IDLE,            // Perustila ilman ajastusta
  PRGM_STATE_SET_CLOCK,       // Kellonasetustila
  PRGM_STATE_SET_TIMER,       // Ajastustila ->
  PRGM_STATE_SET_LENGTH,      // Pituuden asetustila
  PRGM_STATE_TIMED,           // Ajastus asetettu
  PRGM_STATE_BURN_BABY_BURN,  // Ajastimen laukaisu
  PRGM_STATE_ACTIVE           // Rele aktiivisena
} PRGM_STATE_S;               // Ohjelman päätilakone

typedef enum {
  DISP_STATE_TIME,            // Kellonäyttö
  DISP_STATE_SET_TIME,        // Kellonsäätö
  DISP_STATE_SET_TIMER,       // Ajastuksensäätö
  DISP_STATE_SET_LENGTH       // Pituudensäätö
} DISP_STATE_S;               // Näyttötilat

typedef enum {
  LED1_STATE_BLINKING,         // 1s vilkku
  LED1_STATE_DOUBLE,           // 2s tuplavilkku
  LED1_STATE_CONSTANT          // Kiinteä
} LED1_STATE_S;                // LED-tilat

// ****************************************************************************



// MUUTTUJAT
PRGM_STATE_S programState = PRGM_STATE_IDLE;  // Ohjelman tila
PRGM_STATE_S previousState = programState;    // Ohjelman edellinen tila (käytetään, kun palataan esim. aktiivitilasta ajastustilaan)
DISP_STATE_S displayState = DISP_STATE_TIME;  // Näytön tila
LED1_STATE_S led1State = LED1_STATE_BLINKING; // Ledin tila

int digitBuffer[4] = {0, 0, 0, 0};          // Näytönohjaimen puskuri, tähän ohjataan muut xDigits-muuttujat, jotka halutaan näytölle
int clockDigits[4] = {0, 0, 0, 0};          // Kellonaika
int clockSetupDigits[4] = {0, 0, 0, 0};     // Väliaikainen muuttuja kellonasetukseen
int timerSetupDigits[4] = {0, 0, 0, 0};     // Väliaikainen muuttuja ajastuksen asetukseen
int readyDigits[4] = {9, 9, 9, 9};          // Asetettu valmistumisaika
int previousReadyDigits[4] = {9, 9, 9, 9};  // Aiempi valmistumisaika (käytetään manuaalikäynnistyksessä ajastuksen tallennukseen)
int lengthDigits[4] = {0, 0, 0, 0};         // Ajastuksen pituusasetus
int startTimeDigits[4] = {0, 0, 0, 0};      // Aloitusaika (= ready - length)

int lengthIndex = 0;              // Valittu pituusindeksi (LENGTHS[lengthIndex])
int digitScan = 0;                // Näytönohjaimen näyttövalintamuuttuja
int plusBtnState;                 // Plusnapin tila
int plusBtnStateLast = LOW;       // Plusnapin edellinen tila
int minusBtnState;                // Miinusnapin tila
int minusBtnStateLast = LOW;      // Miinusnapin edellinen tila
int enterBtnState;                // Enternapin tila
int enterBtnStateLast = LOW;      // Enternapin edellinen tila

int s = 0;                        // Sekuntimuuttuja

long lastDebounceTime1 = 0;       // Debounceaika (plus)
long lastDebounceTime2 = 0;       // Debounceaika (miinus)
long lastDebounceTime3 = 0;       // Debounceaika (enter)
long menuTime = 0;                // Menuaika (valikoiden inaktiivisuusajan laskenta)

bool HHmm = true;                 // Eri kellonsäädöissä käytetty lippu tunnit -> minuutit
bool checkedAuto = false;         // Automaattiasetuksen tarkistuslippu (puolen tunnin välein yksi tarkistus)
bool manualStart = false;         // Manuaalikäynnistyksen tarkistuslippu

bool checkPlus = false;           // Plusnappi painettu?
bool checkMinus = false;          // Miinusnappi painettu?
bool checkEnter = false;          // Enternappi painettu?

bool settingMinutes = false;      // Säädetäänkö minuutteja? (näytöt vilkkuu, jos säädetään)
bool settingHours = false;        // Säädetäänkö tunteja? (näytöt vilkkuu, jos säädetään)
// ****************************************************************************


/*
 * Arduino setup
 *
 */
void setup();

/*
 * updateDisp
 * Näytönohjain 7-segmenteille
 */
void updateDisp();

/*
 * checkClock
 * Kellofunktio eri kellonäyttötilojen logiikkaan 24h-kellon mukaan
 * Tekee muutokset suoraan pointerina tuotuun taulukkoon
 */
 void checkClock(int * returnDigits);

/*
 * ISR
 * Keskeytyskäsittelijä ajastimelle
 * Lisää kelloon sekunnin
 */
ISR(TIMER1_COMPA_vect);

/*
 * stateMachine
 * Tilakoneet ohjelmalle, näytölle ja vilkkuledille
 * Valikkotiloista PRGM_STATE_SET_CLOCK, PRGM_STATE_SET_TIMER ja PRGM_STATE_SET_LENGTH paluu edelliseen tilaan (IDLE / TIMED),
 * jos 5s ilman painalluksia. Enter hyväksyy tehdyt valinnat.
 *
 * SET_TIMER -tilasta siirrytään enterillä SET_LENGTH -tilaan. SET_TIMER asettaa ajan, jolloin auton pitää olla lämmin.
 *
 * SET_CLOCK ja SET_TIMER -tiloissa logiikka tunnit (plus/miinus) <Enter> minuutit (plus/miinus) <Enter>
 *
 * IDLE ja TIMED -tiloista voi enterillä käynnistää lämmityksen manuaalisesti 4h maksimilla.
 *
 * ACTIVE-tila päättyy, kun valmistumisaika saavutetaan tai manuaalisesti enterillä. Asetettu ajastus jää voimaan seuraavaa päivää varten, vaikka olisi käynnistetty manuaalisesti.
 */
void stateMachine();

/*
 * getAutomaticValues
 * Asettaa automaattiasetuksella pituuden lämpötilan mukaan ja pyytää aloitusajan päivittämisen
 */
void getAutomaticValues();

/*
 * getStartTime
 * Asettaa aloitusajan (readyDigits - lengthDigits)
 * Tarkistaa aloitusajan logiikan
 */
void getStartTime();

/*
 * readButtons
 * Lukee mikrokytkimien tilat, hoitaa värähtelynpoiston
 */
void readButtons();


// MAIN LOOP
void loop(){

  stateMachine();
  updateDisp();
  delay(2);
}
// ****************************************************************************


// SETUP
void setup(){

  for(int i=0;i<4;i++)
    pinMode(ANODES[i],OUTPUT);

  pinMode(LATCH, OUTPUT);
  pinMode(CLOCK, OUTPUT);
  pinMode(DATA, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(RELAY, OUTPUT);

  pinMode(PLUSBTN, INPUT);
  pinMode(MINUSBTN, INPUT);
  pinMode(ENTERBTN, INPUT);

  cli();

  // 1Hz keskeytysväli, keskeytyskäsittelijä ISR(TIMER1_COMPA_vect)
  TCCR1A = 0;
  TCCR1B = 0;
  OCR1A = 15624;
  TCCR1B |= (1 << WGM12);
  TCCR1B |= (1 << CS12) | (1 << CS10);
  TIMSK1 |= (1 << OCIE1A);

  sei();
}
// ****************************************************************************

// UPDATEDISP
void updateDisp(){
  for(byte j=0; j<4; j++)
      digitalWrite(ANODES[j], LOW);
      
    digitalWrite(LATCH, LOW);
    shiftOut(DATA, CLOCK, MSBFIRST, B11111111);
    digitalWrite(LATCH, HIGH);
    delayMicroseconds(100);
    
    // Jos säädetään tunteja, joka toinen sekunti näytöt 0 & 1 pimeäksi, paitsi jos nappeja painellaan 
    if(settingHours && digitScan <=1 && s % 2 == 0 && digitalRead(PLUSBTN) == LOW && digitalRead(MINUSBTN) == LOW)
      digitalWrite(ANODES[digitScan], LOW);

    // Jos säädetään minuutteja, joka toinen sekunti näytöt 2 & 3 pimeäksi, paitsi jos nappeja painellaan 
    else if(settingMinutes && digitScan >=2 && s % 2 == 0 && digitalRead(PLUSBTN) == LOW && digitalRead(MINUSBTN) == LOW)
      digitalWrite(ANODES[digitScan], LOW);

    // Muut näytöt aktiivisena
    else digitalWrite(ANODES[digitScan], HIGH);
        
    digitalWrite(LATCH, LOW);
    
    if(digitScan==2)
      shiftOut(DATA, CLOCK, MSBFIRST, ~(DIGITS[digitBuffer[digitScan]] | B10000000));
      
    else
      shiftOut(DATA, CLOCK, MSBFIRST, ~DIGITS[digitBuffer[digitScan]]);
    
    digitalWrite(LATCH, HIGH);
    digitScan++;
    
    if(digitScan>3) digitScan=0;
}
// ****************************************************************************

// CHECKCLOCK
void checkClock(int * returnDigits) {

  static int shadowSeconds = 0;                 // Varjosekunnit (jos funktio käsittelee jotain muuta kuin kellonaikaa keskeytyksen aikana)

  int m = returnDigits[3],
      tm = returnDigits[2],
      h = returnDigits[1],
      th = returnDigits[0];

  if(s >= 60 && returnDigits == clockDigits) { // Jos minuutti täynnä & kellonaika käsittelyssä
    m += 1;                 // Lisätään minuutti
    s = 0 + shadowSeconds;  // Nollataan sekunnit ja lisätään varjosekunnit uuteen aikaan
    shadowSeconds = 0;      // Nollataan varjosekunnit
  }

  else if(s >= 60) {        // Jos minuutti täynnä ja kellonaika ei käsittelyssä
    shadowSeconds += 1;     // Lisätään varjosekunti
  }

  if(m > 9) {               // 10 minuuttia täynnä?
    tm += 1;                // Lisätään kymmenminuutteja
    m = 0;                  // Nollataan minuutit
  }

  if(tm >= 6) {             // Tunti täynnä?
    if(returnDigits == clockDigits)
      h += 1;               // Lisätään tunti, jos käsitellään kelloa
    tm = tm-6;              // Nollataan kymmenminuutit ja lisätään ylimenevät
  }

  if(h > 9) {               // 10h täynnä?
    th += 1;                // Lisätään kymmentunti
    h = h-10;               // Nollataan tunnit ja lisätään ylimenevät
  }

  if(th == 2 && h == 4) {   // Vuorokausi täynnä?
    th = 0;                 // Nollataan kymmenet tunnit
    h = 0;                  // Nollataan tunnit
  }

  if(m < 0) {               // Minuutit alle 0?
    tm -= 1;                // Vähennetään kymmenminuutti
    m = 9;                  // Minuutit -> 9
  }

  if(tm < 0) {              // Kymmenminuutit alle 0?
    // h -= 1;                 // Vähennetään tunti
    tm = 5 - (tm + 1);      // Kymmenminuutit -> 5, vähennetään ylimääräiset
  }

  if(h < 0) {               // Tunnit alle 0?
    th -= 1;                // Vähennetään kymmentunti
    h = 9 + (h + 1);        // Tunnit -> 9, vähennetään ylimääräiset
  }

  if(th < 0) {              // Kymmenet tunnit alle 0?

    if(h <= -1) {           // Tunnit miinuksella enemmän kuin 1?
      h = 4 + h;            // Uudet tunnit (24 - h)
    }

    else {
      h = 3;
    }

    th = 2;                 // Tunnit -> 23
  }

  returnDigits[0] = th;     // Palautetaan kymmenet tunnit alkuperäiseen taulukkoon
  returnDigits[1] = h;      // Palautetaan tunnit alkuperäiseen taulukkoon
  returnDigits[2] = tm;     // Palautetaan kymmenet minuutit alkuperäiseen taulukkoon
  returnDigits[3] = m;      // Palautetaan minuutit alkuperäiseen taulukkoon
}
// ****************************************************************************

// AJASTINKESKEYTYS
ISR(TIMER1_COMPA_vect) {

  // Lisää kelloon sekunnin
  s += 1;
}
// ****************************************************************************

// STATEMACHINE
void stateMachine() {

  int timeArray[5] = {1000, 1200, 1400, 1600, 1999};   // Tuplavilkun ajat
    
  readButtons();          // Luetaan mikrokytkimet

  switch(programState) {  // Ohjelman tilakone switch-case

    /*
     * IDLE
     *
     * Ei ajastusta asetettu.
     * Plus-nappi siirtää kellonsäätötilaan
     * Miinusnappi siirtää ajastustilaan
     * Enternappi käynnistää lämmityksen heti (manuaalipoisto / max 4h)
     */

    case PRGM_STATE_IDLE:

      displayState = DISP_STATE_TIME;         // Valitaan näyttötila
      led1State = LED1_STATE_BLINKING;        // Valitaan led-tila

      if(checkPlus) {                         // Onko plus painettu?

        programState = PRGM_STATE_SET_CLOCK;  // Asetetaan ohjelman tuleva tila (kellonasetus)
        previousState = PRGM_STATE_IDLE;      // Asetetaan lähtötila, johon palataan

        settingHours = true;
        menuTime = millis();                  // Asetetaan valikon alkuaika
        checkPlus = false;                    // Nollataan kytkimen lippu

        clockSetupDigits[0] = clockDigits[0]; // Kopioidaan nykyinen kellonaika asetusmuuttujaan
        clockSetupDigits[1] = clockDigits[1];
        clockSetupDigits[2] = clockDigits[2];
        clockSetupDigits[3] = clockDigits[3];
      }

      else if(checkMinus) {                   // Onko miinus painettu?

        programState = PRGM_STATE_SET_TIMER;  // Asetetaan ohjelman tuleva tila (ajastuksen asetus)
        previousState = PRGM_STATE_IDLE;      // Asetetaan lähtötila, johon palataan

        settingHours = true;
        menuTime = millis();                  // Asetetaan valikon alkuaika
        checkMinus = false;                   // Nollataan kytkimen lippu
      }

      else if(checkEnter) {                   // Onko enter painettu? (Manuaalikäynnistys)

        digitalWrite(RELAY, HIGH);            // Asetetaan rele aktiiviseksi
        programState = PRGM_STATE_ACTIVE;     // Asetetaan ohjelman tuleva tila (aktiivitila)
        previousState = PRGM_STATE_IDLE;      // Asetetaan lähtötila, johon palataan

        previousReadyDigits[0] = readyDigits[0];  // Otetaan nykyinen ajastus talteen
        previousReadyDigits[1] = readyDigits[1];
        previousReadyDigits[2] = readyDigits[2];
        previousReadyDigits[3] = readyDigits[3];

        readyDigits[0] = clockDigits[0];
        readyDigits[1] = clockDigits[1] + 4;  // Asetetaan valmistumisaika (nykyhetki + 4h)
        readyDigits[2] = clockDigits[2];
        readyDigits[3] = clockDigits[3];

        checkClock(readyDigits);              // Tarkistetaan valmistumisajan 24h-logiikka

        manualStart = true;                   // Manuaalikäynnistys käytössä
        checkEnter = false;                   // Nollataan kytkimen lippu
      }

      break;


    /*
     * SET CLOCK
     *
     * Kellonsäätö.
     * Plus-kytkin lisää tunteja / minuutteja kellonaikaan, riippuen HHmm-lipusta.
     * Miinuskytkin vähentää tunteja / minuutteja kellonajasta, riippuen HHmm-lipusta.
     * Enter vaihtaa HHmm lipun käänteiseksi (vaihtaa minuuttien ja tuntien välillä)
     * 5s ilman napinpainalluksia palauttaa edellisen käytetyn tilan.
     */

    case PRGM_STATE_SET_CLOCK:

      displayState = DISP_STATE_SET_TIME;     // Valitaan näyttötila
      led1State = LED1_STATE_CONSTANT;        // Valitaan led-tila
           
      if(checkPlus) {                         // Onko plus painettu?

        if(HHmm)                              // Säädetäänkö tunteja?
          clockSetupDigits[1]++;              // Yksi tunti lisää

        else clockSetupDigits[3]++;           // Yksi minuutti lisää

        checkPlus = false;                    // Nollataan kytkimen lippu
        menuTime = millis();                  // Nollataan valikkoaika
      }

      else if(checkMinus) {                   // Onko miinus painettu?

        if(HHmm)                              // Säädetäänkö tunteja?
          clockSetupDigits[1]--;              // Tunti vähemmäksi

        else clockSetupDigits[3]--;           // Minuutti vähemmäksi

        menuTime = millis();                  // Nollataan valikkoaika
        checkMinus = false;                   // Nollataan kytkimen lippu
      }

      else if(checkEnter) {                   // Onko enter painettu?

        if(HHmm) {
          HHmm = false;
          menuTime = millis();
          settingHours = false;
          settingMinutes = true;
          checkEnter = false;
        }

        else if(!HHmm) {

          clockDigits[0] = clockSetupDigits[0];
          clockDigits[1] = clockSetupDigits[1];
          clockDigits[2] = clockSetupDigits[2];
          clockDigits[3] = clockSetupDigits[3];

          s = 0;

          programState = previousState;
          menuTime = 0;
          HHmm = true;
          checkEnter = false;
          settingMinutes = false;
        }
      }

      // Jos ilman toimintoja 5s, palataan edelliseen tilaan
      if((millis() - menuTime) > MENUDELAY) {

        programState = previousState;
        HHmm = true;
        menuTime = 0;
        settingMinutes = false;
        settingHours = false;
      }

      break;


    /*
     * SET TIMER
     *
     * Ajastuksensäätö.
     * HH -> Enter -> mm -> Enter -> SET_LENGTH
     * 5s ilman napinpainalluksia palauttaa edellisen käytetyn tilan.
     */

    case PRGM_STATE_SET_TIMER:

      displayState = DISP_STATE_SET_TIMER;
      led1State = LED1_STATE_CONSTANT;

      if(checkPlus) {

        if(HHmm)
          timerSetupDigits[1]++;

        else timerSetupDigits[2]++;

        checkPlus = false;
        menuTime = millis();
      }

      else if(checkMinus) {

        if(HHmm)
          timerSetupDigits[1]--;

        else timerSetupDigits[2]--;

        checkMinus = false;
        menuTime = millis();
      }

      if(checkEnter && HHmm) {

        HHmm = !HHmm;
        checkEnter = false;
        menuTime = millis();
        settingHours = false;
        settingMinutes = true;
      }

      else if(checkEnter && !HHmm) {

        programState = PRGM_STATE_SET_LENGTH;

        readyDigits[0] = timerSetupDigits[0];
        readyDigits[1] = timerSetupDigits[1];
        readyDigits[2] = timerSetupDigits[2];
        readyDigits[3] = timerSetupDigits[3];

        HHmm = true;
        menuTime = millis();
        checkEnter = false;
        settingHours = true;
        settingMinutes = true;

        break;
      }

      if((millis() - menuTime) > MENUDELAY) {

        programState = previousState;
        displayState = DISP_STATE_TIME;

        menuTime = 0;
        HHmm = true;
        settingHours = false;
        settingMinutes = false;
      }

      break;


    /*
     * SET LENGTH
     *
     * Ajastuksen pituuden säätö
     */

    case PRGM_STATE_SET_LENGTH:

      displayState = DISP_STATE_SET_LENGTH;   // Valitaan näyttötila
      led1State = LED1_STATE_CONSTANT;        // Valitaan led-tila

      if(checkPlus) {

        // Ennalta-asetetut pituudet taulukossa LENGTHS[]
        lengthIndex++;

        if(lengthIndex > 8)
          lengthIndex = 0;

        checkPlus = false;
        menuTime = millis();
      }

      else if(checkMinus) {

        lengthIndex--;

        if(lengthIndex < 0)
          lengthIndex = 8;

        checkMinus = false;
        menuTime = millis();
      }

      if(checkEnter) {

        if(lengthIndex == 0) {
          programState = previousState;
          break;
        }
        
        if(lengthIndex == 8) {

          // Automaattitila
          getAutomaticValues();
        }

        else {

          // Haetaan taulukosta valitun keston eri luvut pituudeksi
          lengthDigits[0] = LENGTHS[lengthIndex][0];
          lengthDigits[1] = LENGTHS[lengthIndex][1];
          lengthDigits[2] = LENGTHS[lengthIndex][2];
          lengthDigits[3] = LENGTHS[lengthIndex][3];
        }
      
        programState = PRGM_STATE_TIMED;
        displayState = DISP_STATE_TIME;

        checkEnter = false;
        settingHours = false;
        settingMinutes = false;

        // Päivitetään aloitusaika
        getStartTime();
      }

      if((millis() - menuTime) > MENUDELAY) {

        programState = previousState;
        displayState = DISP_STATE_TIME;
        
        settingHours = false;
        settingMinutes = false;

        menuTime = 0;
      }

      break;


    /*
     * TIMED
     *
     * Ajastus asetettu
     *
     */

    case PRGM_STATE_TIMED:

      displayState = DISP_STATE_TIME;       // Valitaan näyttötila
      led1State = LED1_STATE_DOUBLE;        // Valitaan led-tila

      if(clockDigits[2] == 0 || clockDigits[2] == 3) {
        
        if(lengthIndex == 8 && !checkedAuto) {
          
          getAutomaticValues();
          checkedAuto = true;
          getStartTime();
        }
      }

      else checkedAuto = false;

      if(clockDigits[0] == startTimeDigits[0] &&
        clockDigits[1] == startTimeDigits[1] &&
        clockDigits[2] == startTimeDigits[2] &&
        clockDigits[3] == startTimeDigits[3]) {

        previousState = programState;
        programState = PRGM_STATE_BURN_BABY_BURN;
      }

      if(checkPlus) {

        programState = PRGM_STATE_SET_CLOCK;
        previousState = PRGM_STATE_TIMED;

        checkPlus = false;
        menuTime = millis();

        clockSetupDigits[0] = clockDigits[0];
        clockSetupDigits[1] = clockDigits[1];
        clockSetupDigits[2] = clockDigits[2];
        clockSetupDigits[3] = clockDigits[3];
      }

      else if(checkMinus) {

        programState = PRGM_STATE_SET_TIMER;
        previousState = PRGM_STATE_TIMED;

        checkMinus = false;
        menuTime = millis();
      }

      else if(checkEnter) {

        digitalWrite(RELAY, HIGH);
        programState = PRGM_STATE_ACTIVE;
        previousState = PRGM_STATE_TIMED;

        previousReadyDigits[0] = readyDigits[0];
        previousReadyDigits[1] = readyDigits[1];
        previousReadyDigits[2] = readyDigits[2];
        previousReadyDigits[3] = readyDigits[3];

        readyDigits[0] = clockDigits[0];
        readyDigits[1] = clockDigits[1] + 4;
        readyDigits[2] = clockDigits[2];
        readyDigits[3] = clockDigits[3];

        checkClock(readyDigits);

        manualStart = true;
        checkEnter = false;
      }

      break;


    /*
     * BURN, BABY, BURN (Master ignition routine) :)
     *
     * Ajastimen laukaisu, tähän tilaan ei voi palata
     *
     */

    case PRGM_STATE_BURN_BABY_BURN:

      digitalWrite(RELAY, HIGH);
      programState = PRGM_STATE_ACTIVE;
      displayState = DISP_STATE_TIME;

      break;


    /*
     * ACTIVE
     *
     * Releen aktiivitila. Valmistumisajan saavuttaminen tai enter-painallus palauttaa edellisen tilan (TIMED / IDLE)
     *
     */

    case PRGM_STATE_ACTIVE:

      displayState = DISP_STATE_TIME;         // Valitaan näyttötila
      led1State = LED1_STATE_BLINKING;        // Valitaan led-tila

      // Jos valmistumisaika saavutettu tai enter painettu
      if((clockDigits[0] == readyDigits[0] &&
        clockDigits[1] == readyDigits[1] &&
        clockDigits[2] == readyDigits[2] &&
        clockDigits[3] == readyDigits[3] ) || checkEnter) {

        // Jos käynnistetty manuaalisesti, palautetaan aiempi ajastus voimaan
        if(manualStart) {

          readyDigits[0] = previousReadyDigits[0];
          readyDigits[1] = previousReadyDigits[1];
          readyDigits[2] = previousReadyDigits[2];
          readyDigits[3] = previousReadyDigits[3];

          manualStart = false;
        }

        digitalWrite(RELAY, LOW);
        programState = previousState;

        if(checkEnter)
          checkEnter = false;
      }

      break;

    default:
      break;
  }

  // Näytön tilakone
  switch(displayState) {

    // Kellonäyttö
    case DISP_STATE_TIME:

      checkClock(clockDigits);
      digitBuffer[0] = clockDigits[0];
      digitBuffer[1] = clockDigits[1];
      digitBuffer[2] = clockDigits[2];
      digitBuffer[3] = clockDigits[3];
      break;

    // Kellonsäätönäyttö
    case DISP_STATE_SET_TIME:

      checkClock(clockSetupDigits);
      digitBuffer[0] = clockSetupDigits[0];
      digitBuffer[1] = clockSetupDigits[1];
      digitBuffer[2] = clockSetupDigits[2];
      digitBuffer[3] = clockSetupDigits[3];
      break;

    // Ajastuksensäätönäyttö
    case DISP_STATE_SET_TIMER:

      checkClock(timerSetupDigits);
      digitBuffer[0] = timerSetupDigits[0];
      digitBuffer[1] = timerSetupDigits[1];
      digitBuffer[2] = timerSetupDigits[2];
      digitBuffer[3] = timerSetupDigits[3];
      break;

    case DISP_STATE_SET_LENGTH:

      // Pituudensäätönäyttö
      if(lengthIndex != 8) {
        checkClock(lengthDigits);
      }

      digitBuffer[0] = LENGTHS[lengthIndex][0];
      digitBuffer[1] = LENGTHS[lengthIndex][1];
      digitBuffer[2] = LENGTHS[lengthIndex][2];
      digitBuffer[3] = LENGTHS[lengthIndex][3];
      
      break;

    default:
      break;
  }

  // Vilkkuledin tilakone
  switch(led1State) {

    static long ledBlinkTime = millis();

    // Kiinteä
    case LED1_STATE_CONSTANT:
    
      digitalWrite(LED1, HIGH);
      break;

    // Normaalivilkku
    case LED1_STATE_BLINKING:

      if(s % 2 == 0)
        digitalWrite(LED1, HIGH);

      else digitalWrite(LED1, LOW);

      break;

    // Tuplavilkku
    case LED1_STATE_DOUBLE:
    
      if((millis() - ledBlinkTime) < timeArray[0]) {
        digitalWrite(LED1, LOW);
      }

      if((millis() - ledBlinkTime) > timeArray[0] &&
          (millis() - ledBlinkTime) < timeArray[1]) {
        digitalWrite(LED1, HIGH);
      }

      if((millis() - ledBlinkTime) > timeArray[1] && 
      (millis() - ledBlinkTime) < timeArray[2]) {
        digitalWrite(LED1, LOW);
      }

      if((millis() - ledBlinkTime) > timeArray[2] &&
          (millis() - ledBlinkTime) < timeArray[3]) {
        digitalWrite(LED1, HIGH);
      }

      if((millis() - ledBlinkTime) > timeArray[3] && 
      (millis() - ledBlinkTime) < timeArray[4]) {
        digitalWrite(LED1, LOW);
      }

      if((millis() - ledBlinkTime) > timeArray[4])
        ledBlinkTime = millis();

      break;

    default:
      break;
  }

}
// ****************************************************************************

// GETAUTOMATICVALUES
void getAutomaticValues() {

  // Lue lämpötila & vertaa taulukkoon & määritä lengthDigits
  float voltage = analogRead(TMPSENSOR);

  if(voltage < 0.6) {
    
    lengthDigits[0] = 0;
    lengthDigits[1] = 1;
    lengthDigits[2] = 0;
    lengthDigits[3] = 0;
  }

  if(voltage < 0.4) {
    
    lengthDigits[0] = 0;
    lengthDigits[1] = 2;
    lengthDigits[2] = 0;
    lengthDigits[3] = 0;    
  }

  if(voltage < 0.35) {
    
    lengthDigits[0] = 0;
    lengthDigits[1] = 3;
    lengthDigits[2] = 0;
    lengthDigits[3] = 0;
  }
  
  if(voltage < 0.3) {
    
    lengthDigits[0] = 0;
    lengthDigits[1] = 4;
    lengthDigits[2] = 0;
    lengthDigits[3] = 0;
  }  
}
// ****************************************************************************

// GETSTARTTIME
void getStartTime() {

  // Aktivointiaika = valmistumisaika - lämmityksen kesto
  startTimeDigits[0] = readyDigits[0] - lengthDigits[0];
  startTimeDigits[1] = readyDigits[1] - lengthDigits[1];
  startTimeDigits[2] = readyDigits[2] - lengthDigits[2];
  startTimeDigits[3] = readyDigits[3] - lengthDigits[3];


  // Sitten alkaa tarkistelu, tämä ei ole se maailman elegantein funktio...
  if(startTimeDigits[1] < 0 || startTimeDigits[2] < 0) {

    if(startTimeDigits[2] < 0) {

     startTimeDigits[1] -= 1;
       
     if(startTimeDigits[2] == -1)
       startTimeDigits[2] = 5;
        
     if(startTimeDigits[2] == -2)
       startTimeDigits[2] = 4;
        
     if(startTimeDigits[2] == -3)
       startTimeDigits[2] = 3;
    }

    if(startTimeDigits[1] < 0) {

      startTimeDigits[0] -=1;
      
      if(startTimeDigits[1] == -1 && startTimeDigits[0] >= 0)
        startTimeDigits[1] = 9;

      if(startTimeDigits[1] == -2 && startTimeDigits[0] >= 0)
        startTimeDigits[1] = 8;

      if(startTimeDigits[1] == -3 && startTimeDigits[0] >= 0)
        startTimeDigits[1] = 7;

      if(startTimeDigits[1] == -4 && startTimeDigits[0] >= 0)
        startTimeDigits[1] = 6;
    }

    if(startTimeDigits[0] < 0) {

      startTimeDigits[0] = 2;

      if(startTimeDigits[1] == -1)
        startTimeDigits[1] = 3;

      if(startTimeDigits[1] == -2)
        startTimeDigits[1] = 2;

      if(startTimeDigits[1] == -3)
        startTimeDigits[1] = 1;

      if(startTimeDigits[1] == -4)
        startTimeDigits[1] = 0;
    }
    
  }

}
// ****************************************************************************

// READBUTTONS
void readButtons() {

  // Luetaan mikrokytkimet
  int reading1 = digitalRead(PLUSBTN);
  int reading2 = digitalRead(MINUSBTN);
  int reading3 = digitalRead(ENTERBTN);

  if(reading1 != plusBtnStateLast)
    lastDebounceTime1 = millis();

  if(reading2 != minusBtnStateLast)
    lastDebounceTime2 = millis();

  if(reading3 != enterBtnStateLast)
    lastDebounceTime3 = millis();

  // Liput ylös, kun värähtely tasaantunut
  if((millis() - lastDebounceTime1) > DEBOUNCEDELAY) {

    if(reading1 != plusBtnState) {

      plusBtnState = reading1;

      if(plusBtnState == HIGH)
        checkPlus = true;
    }
  }

  if((millis() - lastDebounceTime2) > DEBOUNCEDELAY) {

    if(reading2 != minusBtnState) {

      minusBtnState = reading2;

      if(minusBtnState == HIGH)
        checkMinus = true;
    }
  }

  if((millis() - lastDebounceTime3) > DEBOUNCEDELAY) {

    if(reading3 != enterBtnState) {

      enterBtnState = reading3;

      if(enterBtnState == HIGH)
        checkEnter = true;
    }
  }

  plusBtnStateLast = reading1;
  minusBtnStateLast = reading2;
  enterBtnStateLast = reading3;

}
// ****************************************************************************
